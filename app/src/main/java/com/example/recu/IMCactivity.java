package com.example.recu;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class IMCactivity extends AppCompatActivity {
    private EditText etHeightMeters, etHeightCm, etWeight;
    private TextView tvResult, tvWelcome, tvUserName;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String NAME_KEY = "nameKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imcactivity);

        etHeightMeters = findViewById(R.id.etHeightMeters);
        etHeightCm = findViewById(R.id.etHeightCm);
        etWeight = findViewById(R.id.etWeight);
        tvResult = findViewById(R.id.tvResult);
        tvWelcome = findViewById(R.id.tvWelcome);
        tvUserName = findViewById(R.id.tvUserName);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String name = sharedPreferences.getString(NAME_KEY, "");
        tvUserName.setText(name);
    }

    public void calculateIMC(View view) {
        double heightMeters = Double.parseDouble(etHeightMeters.getText().toString());
        double heightCm = Double.parseDouble(etHeightCm.getText().toString());
        double weight = Double.parseDouble(etWeight.getText().toString());

        double height = heightMeters + (heightCm / 100);
        double imc = weight / (height * height);

        tvResult.setText(String.format("IMC: %.2f kg/m²", imc));
    }

    public void clearFields(View view) {
        etHeightMeters.setText("");
        etHeightCm.setText("");
        etWeight.setText("");
        tvResult.setText("");
    }
}
