package com.example.recu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText etName;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String NAME_KEY = "nameKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = findViewById(R.id.etName);

        // Clear saved name when starting the application
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NAME_KEY, "");
        editor.apply();
    }

    public void openIMC(View view) {
        saveName();
        Intent intent = new Intent(this, IMCactivity.class);
        startActivity(intent);
    }

    public void openConversion(View view) {
        saveName();
        //Intent intent = new Intent(this, ConversionActivity.class);
        //startActivity(intent);
    }

    private void saveName() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(NAME_KEY, etName.getText().toString());
        editor.apply();
    }
}
